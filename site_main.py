import os

from flask import Flask, request, render_template
from werkzeug.utils import secure_filename

app = Flask( __name__ ) 
app.config['UPLOAD_PATH'] = 'data/'

@app.route("/test2")
def index():
    return render_template("index.html")

@app.route("/load_stl.min.js" )
def get_lib():
    return open( 'static/load_stl.min.js' ).read()

@app.route("/parser.min.js" )
def get_lib2():
    return open( 'static/parser.min.js' ).read()


@app.route( '/vec2leg', methods=[ 'GET', 'POST' ] )
def upload():
    return render_template( 'model_viewer.html' )

    if request.method == 'POST':
        for f in request.files.getlist( 'files' ):
            filename = secure_filename( f.filename )
            f.save( os.path.join( app.config[ 'UPLOAD_PATH' ], filename ) )
        return render_template( 'model_viewer.html' )
    return render_template( 'upload.html' )

if __name__ == '__main__':
    app.run( debug=True )